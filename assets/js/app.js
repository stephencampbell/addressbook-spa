// Modules
import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import VueRouter from 'vue-router';

// Module styles
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Vue components
import Frame from './components/frame';
import Home from './components/home';
import AddressBook from './components/addressbook';
import Contact from './components/contact';

// Images
const imagesContext = require.context('../img', true, /\.(png|jpg|jpeg|gif|ico|webp)$/);
imagesContext.keys().forEach(imagesContext);

// Fonts
const webfontsContext = require.context('../webfonts', true, /\.(ttf|eot|svg|woff|woff2)$/);
webfontsContext.keys().forEach(webfontsContext);

// Routes
const routes = [
  { path: '/', component: Home, meta: { title: "Home" } },
  { path: '/addressbook', component: AddressBook, meta: { title: "Address Book" } },
  { path: '/addressbook/:id', component: Contact, meta: { title: "Contact" } }
]

// Vue Router
const router = new VueRouter({
  mode: 'history',
  routes // short for `routes: routes`
})

// This callback runs before every route change, including on page load.
router.beforeEach((to, from, next) => {
  // This goes through the matched routes from last to first, finding the closest route with a title.
  const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);

  // Find the nearest route element with meta tags.
  const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
  const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);

  // If a route with a title was found, set the document (page) title to that value.
  if(nearestWithTitle) document.title = nearestWithTitle.meta.title;

  // Remove any stale meta tags from the document using the key attribute we set below.
  Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));

  // Skip rendering meta tags if there are none.
  if(!nearestWithMeta) return next();

  // Turn the meta tag definitions into actual elements in the head.
  nearestWithMeta.meta.metaTags.map(tagDef => {
    const tag = document.createElement('meta');

    Object.keys(tagDef).forEach(key => {
      tag.setAttribute(key, tagDef[key]);
    });

    // We use this to track which meta tags we create, so we don't interfere with other ones.
    tag.setAttribute('data-vue-router-controlled', '');

    return tag;
  })
  // Add the meta tags to the document head.
  .forEach(tag => document.head.appendChild(tag));

  next();
});


// Vue modules
Vue.use(BootstrapVue);
Vue.use(VueRouter);

// Vue
const app = new Vue({
  el: '#vue',
  router,
  render: h => h(Frame)
});