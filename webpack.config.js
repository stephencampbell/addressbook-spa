var Encore = require('@symfony/webpack-encore');

Encore
    // General
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())

    // Entries
    .addEntry('js/app', './assets/js/app.js')
    .addStyleEntry('css/app', './assets/css/app.scss')
    .addStyleEntry('css/fontawesome', './assets/css/fontawesome-all.css')

    // Loaders
    .enableSassLoader()
    .enableVueLoader()
    .configureFilenames({
        images: '[path][name].[ext]',
        fonts: '[path][name].[ext]'
    })
;

module.exports = Encore.getWebpackConfig();
