<?php

namespace App\Controller;

use App\Entity\Contact;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ContactController extends Controller {
    public function addContact(Request $request) {
        $entityManager = $this->getDoctrine()->getManager();

        $data = json_decode($request->getContent());

        $contact = new Contact();
        $contact->setFirstName($data->firstName);
        $contact->setLastName($data->lastName);
        $contact->setBirthday(\DateTime::createFromFormat('Y-m-d', $data->birthday));
        $contact->setMobileNumber($data->mobileNumber);
        $contact->setEmailAddress($data->emailAddress);
        $contact->setImagePath($data->imagePath);

        $entityManager->persist($contact);
        $entityManager->flush();

        $contactData = [];
        $contactData['id']           = $contact->getId();
        $contactData['firstName']    = $contact->getFirstName();
        $contactData['lastName']     = $contact->getLastName();
        $contactData['birthday']     = $contact->getBirthday();
        $contactData['mobileNumber'] = $contact->getMobileNumber();
        $contactData['emailAddress'] = $contact->getEmailAddress();
        $contactData['imagePath']    = $contact->getImagePath();

        return new Response(json_encode($contactData));
    }
    public function removeContact(Request $request) {
        
    }
    public function editContact(Request $request) {
        
    }
    public function loadAllContacts(Request $request) {
        // Load contact data
        $contactRepository = $this->getDoctrine()->getRepository(Contact::class);
        $contactObjects = $contactRepository->findAll();

        // Index contact data
        $contacts = [];
        foreach($contactObjects as $key=>$contact) {
            $contacts[$key] = $this->buildContactArray($contact);
        }

        return new Response(json_encode($contacts));
    }
    public function loadContact(Request $request) {
        $id = json_decode($request->getContent())->id;
        $contactRepository = $this->getDoctrine()->getRepository(Contact::class);
        $contactObject = $contactRepository->find($id);
        $contact = $this->buildContactArray($contactObject);
        return new Response(json_encode($contact));
    }

    private function buildContactArray(Contact $contact) {
        $output['id']           = $contact->getId();
        $output['firstName']    = $contact->getFirstName();
        $output['lastName']     = $contact->getLastName();
        $output['birthday']     = $contact->getBirthday();
        $output['mobileNumber'] = $contact->getMobileNumber();
        $output['emailAddress'] = $contact->getEmailAddress();
        $output['imagePath']    = $contact->getImagePath();
        return $output;
    }
}
