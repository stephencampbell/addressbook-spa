# AddressBook
This project is a single-page application (SPA) designed as a basic showcase of clientside routing and asynchronous data.

## The development process
AddressBook is developed using [Symfony 4](https://symfony.com) for backend logic and [Vue](https://vuejs.org) for the frontend. Vue is accompanied by [Bootstrap Vue](https://bootstrap-vue.js.org) and [Vue Router](https://router.vuejs.org). [SQLite](https://sqlite.org/index.html) is used for the database.

## Requirements
- Node and NPM
- Composer
- PHP
- SQLite3

## To use
1. Clone this repository with `git clone https://stephenjcampbell@bitbucket.org/stephenjcampbell/addressbook-spa.git`
2. Go to your new directory with `cd addressbook-spa`
3. Run `npm install` to install the node modules needed for AddressBook
4. To build local assets, use one of the following:
    - `npm run dev` for a development build
    - `npm run build` for a production build
    - `npm run watch` to automatically rebuild assets while working on them
5. Install PHP prerequisites by running `composer install`
6. Launch the Symfony server, run `php bin/console server:run`